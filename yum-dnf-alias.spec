Name:		yum-dnf-alias
Summary:	A small alias allowing yum to pretend dnf
Summary(ru_RU.UTF-8): Небольшой псевдоним, позволяющий yum притвориться dnf
Version:	1.0.0
Release:	3%{?dist}
License:	Public Domain
URL:		https://gitlab.com/pda_rpms/yum-dnf-alias
Group:		System Environment/Shells
Packager:	Dmitriy Pomerantsev <pda2@yandex.ru>
Source0:	aliases_dnf.sh
Source1:	dnf
BuildArch:	noarch
BuildRoot:	%{_tmppath}/%{name}-%{version}-root
%{?systemd_requires}
Requires:	bash
BuildRequires:	systemd
Conflicts:	dnf

%description
Although Fedora has long since switched to dnf, CentOS 7 continues to
use yum. To simplify package management and given the strong similarity
of commands yum and dnf, this package provides an alias for using the
command dnf instead of yum. Also it contains limited bash's autocompletion.

%description -l ru_RU.UTF-8
Хотя Fedora давно перешла на dnf, CentOS 7 продолжает использовать yum.
Чтобы упростить управление пакетами и, учитывая сильное сходство команд
yum и dnf, этот пакет предоставляет алиас, позволяющий использовать команду
dnf вместо yum. Так же ограниченно поддерживается автодополнение в bash.

%build
# None required

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%{__mkdir_p} %{buildroot}%{_sysconfdir}/profile.d
%{__install} -pm 0644 %{SOURCE0} %{buildroot}%{_sysconfdir}/profile.d/

%{__mkdir_p} %{buildroot}%{_sysconfdir}/bash_completion.d
%{__install} -pm 0644 %{SOURCE1} %{buildroot}%{_sysconfdir}/bash_completion.d/

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%files
%{_sysconfdir}/profile.d/*
%{_sysconfdir}/bash_completion.d/*

%changelog
* Thu Nov 28 2019 Dmitriy Pomerantsev <pda2@yandex.ru> - 1.0.0 - 3
- License changed for Public Domain.

* Sat Dec 02 2018 Dmitriy Pomerantsev <pda2@yandex.ru> - 1.0.0 - 2
- No aliases in plain Bourne shell.

* Fri Nov 23 2018 Dmitriy Pomerantsev <pda2@yandex.ru> - 1.0.0 - 1
- Initial package.
